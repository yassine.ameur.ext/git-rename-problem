# Git Rename Problem

Ce répo est une petite demonstration des limites du `mécanisme de renaming de git`.

### Comment ça marche: 
La notion de renaming n'existe pas vraiment dans le moteur de création de fichiers dans le dossier caché `.git`que git met à jour à chaque fois que vous faites des opérations. Quand on fait `git mv <source> <target>`, git ne fait pas vraiment de renaming, il s'agit bien d'une suppression d'un fichier + création d'un autre.

Lorsque vous faites une merge requests, git applique son algorithme de diff (le fameux `git diff`) pour tracker les différences locales au sein de chaque fichier. 

Afin d'optimiser le nombre d'opérations de changement, avant de finir son travail, l'algorithme de `git diff` applique une heuristique pour chercher les opérations du renaming: S'il trouve qu'un fichier est créé à partir d'un autre suite à un `git mv` et que les changements ne sont pas assez profondes, il va afficher `renamed`, sinon il va les considérer qu'ils sont deux fichiers différents.


# Démonstration:

## Renaming fonctionne:
 - Créer une première branche à partir du master
 - Renommer le fichier `file_1.txt` à `file_2.txt`
 - Ajouter la ligne suivante vers la fin du fichier: `zzzz`
 - Faites un merge request: Il y a aura bien l'opération `rename` et git va lier `file_1.txt` à `file_2.txt`
 
## Renaming qui ne fonctionne pas :
 - Créer une première branche à partir du master
 - Renommer le fichier `file_1.txt` à `file_2.txt`
 - Remplacer la première ligne `xxxx` par les deux lignes suivante:
      
      `wwww`
      
      `zzzz`
 - Ajouter la ligne suivante vers la fin du fichier: `aaaa` 
 - Faites un merge request: Git ne verra aucun lien entre les deux fichiers. Il va considérer que vous avez supprimé `file_1.txt` et créé `file_2.txt`
 


